<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterImageNameColumnToArticlesAndCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->string('bg_image_name')->nullable()->after('image');
        });
        Schema::table('categories', function (Blueprint $table) {
            $table->string('bg_image_name')->nullable()->after('image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->dropColumn('bg_image_name');
        });
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('bg_image_name');
        });
    }
}
