<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $defaultAdmin = [
            'role' => 'admin',
            'name' => 'Default admin',
            'email' => 'admin@locel.com',
            'password' => bcrypt('qwerty'),
            'remember_token' => str_random(10),
        ];

        \App\Models\User::updateOrCreate($defaultAdmin);
    }
}
