<?php

use Illuminate\Database\Seeder;

class SettingGroupsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groups = [
            [
                'id' => 1,
                'name' => 'Application settings'
            ],
            [
                'id' => 2,
                'name' => 'Site settings'
            ]
        ];

        foreach ($groups as $group) {
            \App\Models\SettingGroup::updateOrCreate($group);
        }
    }
}
