<?php

use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = [
            [
                'id'          => 1,
                'group_id'    => 1,
                'name'        => 'Temporary invite code',
                'description' => 'If you will change this code, users with old code will lose access to application',
                'code'        => 'temp_invite_code',
                'value'       => 'uromed-temp',
                'type'        => 'text',
                'sort'        => 0,
            ]
        ];

        foreach ($settings as $setting) {
            \App\Models\Setting::updateOrCreate($setting);
        }
    }
}
