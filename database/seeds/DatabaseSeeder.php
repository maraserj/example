<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        App\Models\User::truncate();
        App\Models\Category::truncate();
        App\Models\Article::truncate();

        $this->call(UserSeeder::class);
        factory(App\Models\User::class, 10)->create();
        factory(App\Models\Category::class, 10)->create()->each(function ($category) {
            $category->articles()->save(factory(App\Models\Article::class)->make());
        });

        $this->call(SettingsSeeder::class);
        $this->call(SettingGroupsSeeder::class);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
