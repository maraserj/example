<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Article::class, function (Faker $faker) {
    $title = $faker->text(70);

    return [
        'slug'        => str_slug($title),
        'image'       => $faker->imageUrl(),
        'description' => $faker->text(1000),
        'is_active'   => rand(0, 1),
        'title'       => $faker->colorName,
        'user_id'     => null,
        'category_id' => null,
    ];
});
