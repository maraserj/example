<?php
return [
    'errors' => [
        '404'                  => 'Not found',
        '401_bad_permissions'  => 'Unauthorized',
        'bad_request'          => 'Bad request',
        '500'                  => 'Internal server error',
        'permission_denied'    => 'Permission denied',
        'Something went wrong' => 'Something went wrong',
    ],

    'deleted'          => 'Success deleted.',
    'leaved'           => 'Success leaved.',
    'saved'            => 'Success saved.',
    'updated'          => 'Success updated.',
    'error'            => 'Error. Try again.',

];
