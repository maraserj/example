<?php
return [
    'errors' => [
        '404'                  => 'Не найдено',
        '401_bad_permissions'  => 'Не авторизирован',
        'bad_request'          => 'Неверные данные запроса',
        '500'                  => 'Внутренняя ошибка сервера',
        'permission_denied'    => 'Доступ запрещен',
        'Something went wrong' => 'Что-то пошло не так',
    ],

    'saved'            => 'Сохранено',
    'updated'          => 'Обновлено',
    'deleted'          => 'удалено',
    'error'            => 'Ошибка',
];
