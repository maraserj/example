@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="">
            <div class="row">
                <form action="{{ route('email-request') }}" method="post" class="form-control-range" role="form">
                    @csrf
                    <div class="form-group">
                        <h1 class="text-center">Запрос на почту</h1>
                    </div>
                    @if (session('success'))
                        <div class="col-sm-12">
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <strong>{{ session('success') }}</strong>
                            </div>
                        </div>
                    @endif

                    <br>
                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-5">
                            <input type="email" name="email" id="email" class="form-control" required="required">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5 col-sm-offset-2">
                            <button type="submit" class="btn btn-primary">Отправить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="">
            <h1 class="text-center">Контакты</h1>
            <p>Yury Rapoport</p>
            <address>
                1333 coates bluff dr apt 933 <br>
                shreveport <br>
                Louisiana <br>
                71104 <br>
                USA <br>
                12014435181 <br>
            </address>
            e-mail: <a href="mailto:rayradvisors@gmail.com">rayradvisors@gmail.com</a>
        </div>
        <div class="">
            <h1 class="text-center">Описание</h1>
            <p>UroGo -  представляет собой универсальный персонализированный гид по подготовке и проведению урологических процедур. В приложении собрана самая полная и современная информация о наиболее частых эндоскопических операциях, применяемых в урологической практике, методах профилактики осложнений, способах обезболивания и секретах максимального успеха и быстрого восстановления.</p>

            <p>На сегодняшний день, достижения науки и техники позволяют добиваться высоких результатов лечения с использованием малоинвазивных технологий. Тем не менее, успех операции зависит уже не только от оборудования и квалификации врача. В современных реалиях возрастает роль самого пациента, поэтому осведомленность и тесное взаимодействие пациента со всем медицинским персоналом являются ключевыми факторами на всех этапах лечения и залогом эффективности.</p>

            <p>В приложении UroGo собраны ключевые указания пациентам, разработанные консилиумом российских и зарубежных врачей при поддержке компании "Замбон Фарма", в строгом соответствии с новейшими международными рекомендациями, а также ответы на часто задаваемые вопросы перед, во время и после операции. Приложение удобно в использовании, сопровождается графическими и видео пособиями. Среди прочего,  приложение поможет вам узнать:</p>

            <ul>
                <li>как правильно подготовиться к конкретной операции</li>
                <li>как привести свое здоровье в порядок, о чем сообщить врачу перед операцией</li>
                <li>какие анализы и исследования необходимо провести</li>
                <li>как избежать осложнений во время операции, в том числе инфекции</li>
                <li>что принести с собой в стационар</li>
                <li>как будет проходит сама операция и наркоз</li>
                <li>что ожидать после операции и, как, восстановиться за самый короткий срок</li>
            </ul>

            <p>и многое другое.</p>
        </div>
    </div>
@endsection
{{--9435--}}
