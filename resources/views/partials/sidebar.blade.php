<div class="sidebar">
    <ul class="sidebar-links">
        <li class="sidebar-link_item">
            <a href="{{ route('admin.users') }}" class="sidebar-link {{ Route::is('admin.users*') ? 'active zambon-bg-color' : '' }}">
                Пользователи
            </a>
        </li>
        <li class="sidebar-link_item">
            <a href="{{ route('admin.categories') }}" class="sidebar-link {{ Route::is('admin.categories*') || Route::is('admin.operations*') ? 'active zambon-bg-color' : '' }}">
                Категории
            </a>
        </li>
        <li class="sidebar-link_item">
            <a href="{{ route('admin.articles') }}" class="sidebar-link {{ Route::is('admin.articles*') ? 'active zambon-bg-color' : '' }}">
                Текст
            </a>
        </li>
        <li class="sidebar-link_item">
            <a href="{{ route('admin.settings') }}" class="sidebar-link {{ Route::is('admin.settings*') ? 'active zambon-bg-color' : '' }}">
                Настройки
            </a>
        </li>
    </ul>
</div>
