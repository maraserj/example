
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Vue filters
 */

Vue.filter('strip_tags', function (value) {
    if (value) {
        let div = document.createElement('div');
        div.innerHTML = value;
        return div.innerText;
    }
});
Vue.filter('truncate', function (value, length) {
    if (value) {
        if (value.length < length) {
            return value;
        }
        length = length - 2;
        return value.substring(0, length) + '..';
    }
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/Example'));
Vue.component('users-index', require('./components/users/UsersIndex'));
Vue.component('categories-index', require('./components/categories/CategoriesIndex'));
Vue.component('category-show', require('./components/categories/CategoryShow'));
Vue.component('operation-show', require('./components/operations/OperationShow'));
Vue.component('articles-index', require('./components/articles/ArticlesIndex'));
Vue.component('articles-show', require('./components/articles/ArticlesShow'));
Vue.component('settings-index', require('./components/settings/SettingsIndex'));

const app = new Vue({
    el: '#app',
});
