<?php

use Illuminate\Foundation\Inspiring;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;
use \Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $imageUpload = new \App\Services\FileUpload\ImageUpload\ImageUpload();
    $articles = \App\Models\Article::all()->each(function ($item) use ($imageUpload) {
        $imageUpload->baseFolder = 'articles/bg';
        if (!str_contains($item->image, 'lorem') && !empty($item->image)) {
            try {
                $item->update(['bg_image_name' => last(explode('/', $item->bg_image_name))]);
            } catch (Exception $e) {
                $this->info($e->getMessage());
            }
        }
    });
    $categories = \App\Models\Category::all()->each(function ($item) use ($imageUpload) {
        $imageUpload->baseFolder = 'categories/bg';
        if (!str_contains($item->image, 'lorem') && !empty($item->image)) {
            try {
                $item->update(['bg_image_name' => last(explode('/', $item->bg_image_name))]);
            } catch (Exception $e) {
                $this->info($e->getMessage());
            }
        }
    });


})->describe('Display an inspiring quote');
