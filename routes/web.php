<?php
/**
 * @var \Illuminate\Routing\Router $router
 */

$router->get('/', function () {
    return view('welcome');
});
$router->post('oauth/login', 'HomeController@getAccessToken')->name('oauth.custom-login');
$router->post('email-request', 'HomeController@emailRequest')->name('email-request');

Auth::routes();

$router->get('/home', 'HomeController@index')->name('home')->middleware('can:admin');
$router->get('/privacy', 'HomeController@privacy')->name('privacy');

$router->get('/users', 'Admin\IndexController@users')->name('admin.users')->middleware('can:admin');
$router->get('/categories', 'Admin\IndexController@categories')->name('admin.categories')->middleware('can:admin');
$router->get('/categories/{id}', 'Admin\IndexController@categoryShow')->name('admin.categories.show')->middleware('can:admin');
$router->get('/operations/{id}', 'Admin\IndexController@operationShow')->name('admin.operations.show')->middleware('can:admin');
$router->get('/articles', 'Admin\IndexController@articles')->name('admin.articles')->middleware('can:admin');
$router->get('/articles/{id}', 'Admin\IndexController@articleShow')->name('admin.articles.show')->middleware('can:admin');
$router->get('/settings', 'Admin\IndexController@settings')->name('admin.settings')->middleware('can:admin');


$router->resource('/admin/users', 'UsersController')->middleware('can:admin');
$router->resource('/admin/categories', 'CategoriesController')->middleware('can:admin');
$router->resource('/admin/articles', 'ArticleController')->middleware('can:admin');
$router->resource('/admin/settings', 'SettingController')->middleware('can:admin');
$router->get('admin/icons', 'Admin\IndexController@getIcons')->middleware('can:admin');
$router->post('admin/icons', 'Admin\IndexController@saveIcon')->middleware('can:admin');
$router->post('admin/images', 'Admin\IndexController@saveArticleImage')->middleware('can:admin');
