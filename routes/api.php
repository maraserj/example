<?php
/**
 * @var \Illuminate\Routing\Router $router
 */


$router->resource('users', 'UsersController');
$router->resource('categories', 'CategoriesController')/*->middleware(['web', 'auth:api'])*/;
$router->resource('articles', 'ArticleController')/*->middleware(['web', 'auth:api'])*/;
$router->resource('settings', 'SettingController')/*->middleware(['web', 'auth:api'])*/;
