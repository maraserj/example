<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Setting extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'group_id',
        'code',
        'value',
        'type',
        'sort',
    ];

    public function settingGroup()
    {
        return $this->belongsTo(SettingGroup::class, 'id', 'group_id');
    }
}
