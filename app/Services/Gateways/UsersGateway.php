<?php

namespace App\Gateways;

use App\Models\User;
use App\Repositories\UserRepository as Repository;
use App\Services\FileUpload\ImageUpload\ImageUpload;
use Illuminate\Http\Request;


class UsersGateway extends Gateway
{
    /**
     * @var ImageUpload
     */
    protected $fileUpload;

    public function __construct(Repository $repository, ImageUpload $fileUpload)
    {
        parent::__construct($repository);
        $this->fileUpload = $fileUpload;
        $this->fileUpload->baseFolder = 'avatars';
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $forever_code = config('app.forever_invite_code');
        if ($data['invite_code'] == $forever_code) {
            $data['forever_access'] = true;
        }
        else {
            $data['forever_access'] = false;
        }

        if (!empty($request->file('avatar', null))) {
            $data['avatar'] = $this->fileUpload->save($request->file('avatar'), 300);
        }

        return $this->repository->create($data);
    }

    public function update(Request $request, string $id)
    {
        $user = User::find($id);
        $data = $request->all();

        if (!empty($request->file('avatar', null))) {
            if (!empty($user->avatar)) {
                $data['avatar'] = $this->fileUpload->save(
                    $request->file('avatar'),
                    300,
                    null,
                    'update',
                    $user->avatar
                );
            } else {
                $data['avatar'] = $this->fileUpload->save($request->file('avatar'), 300);
            }
        }

        return $this->repository->update($id, $data);
    }

    public function destroy(string $id)
    {
        // delete the image files
        $user = User::find($id);
        $this->fileUpload->deleteImage($user->image);

        // delete the record from the database
        return parent::destroy($id);
    }
}
