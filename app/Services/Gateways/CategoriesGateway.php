<?php

namespace App\Gateways;

use App\Models\Category;
use App\Repositories\CategoryRepository as Repository;
use App\Services\FileUpload\ImageUpload\ImageUpload;
use Illuminate\Http\Request;


class CategoriesGateway extends Gateway
{
    /**
     * @var ImageUpload
     */
    protected $fileUpload;

    public function __construct(Repository $repository, ImageUpload $fileUpload)
    {
        parent::__construct($repository);
        $this->fileUpload = $fileUpload;
        $this->fileUpload->baseFolder = 'categories';
    }

    public function get(Request $request)
    {
        $response = $this->repository->with(['children', 'children.articles', 'articles']);

        if ($request->has('parent_id')) {
            $response = $response->where('parent_id', $request->get('parent_id') == 0 ? null: $request->get('parent_id'));
        };
        if (!auth()->check() || auth()->user()->cant('admin')) {
            $response = $response->active();
        }

        return $response->paginate($request->get('perPage', 15));
    }

    public function show(string $id)
    {
        $response = $this->repository->with(['children', 'children.articles', 'articles']);

        if (request()->has('parent_id')) {
            $response = $response->where('parent_id', request()->get('parent_id') == 0 ? null: request()->get('parent_id'));
        };
        return $response->whereId($id)->first();
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['slug'] = str_slug($data['title']);

        if (!empty($request->file('image', null))) {
            $data['image'] = $this->fileUpload->save($request->file('image'));
        }

        return $this->repository->create($data);
    }

    public function update(Request $request, string $id)
    {
        $category = Category::find($id);
        $data = $request->all();

        if (!empty($request->file('image', null))) {
            if (!empty($category->image)) {
                $data['image'] = $this->fileUpload->save(
                    $request->file('image'),
                    null,
                    null,
                    'update',
                    $category->image
                );
            } else {
                $data['image'] = $this->fileUpload->save($request->file('image'), null);
            }
        }

        return $this->repository->update($id, $data);
    }

    public function destroy(string $id)
    {
        $category = Category::find($id);
        $this->fileUpload->deleteImage($category->image);

        parent::destroy($id);
    }
}
