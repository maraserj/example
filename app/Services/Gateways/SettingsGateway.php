<?php

namespace App\Services\Gateways;

use App\Gateways\Gateway;
use App\Repositories\SettingRepository;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;

class SettingsGateway extends Gateway
{
    use ApiResponse;

    protected $repository;

    public function __construct(SettingRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        //
    }

    public function create(array $data = [])
    {
        //
    }

    public function show(string $id)
    {
        //
    }

    public function update(Request $request, string $id)
    {
        //
    }

    public function delete(int $id)
    {
        //
    }

}