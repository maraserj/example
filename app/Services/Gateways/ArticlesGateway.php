<?php

namespace App\Gateways;

use App\Models\Article;
use App\Repositories\ArticleRepository as Repository;
use App\Services\FileUpload\ImageUpload\ImageUpload;
use Illuminate\Http\Request;


class ArticlesGateway extends Gateway
{
    /**
     * @var ImageUpload
     */
    protected $fileUpload;

    public function __construct(Repository $repository, ImageUpload $fileUpload)
    {
        parent::__construct($repository);
        $this->fileUpload = $fileUpload;
        $this->fileUpload->baseFolder = 'articles';
    }

    public function get(Request $request)
    {
        $response = $this->repository->with('category');

        if (auth()->check() && auth()->user()->can('admin')) {
            return $response->paginate($request->get('perPage', 150));
        }

        return $response->active()->paginate($request->get('perPage', 150));
    }

    public function show(string $id)
    {
        return $this->repository->with(['category', 'category.parent'])->whereId($id)->first();
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['slug'] = str_slug($data['title']);

        if (!empty($request->file('image', null))) {
            $data['image'] = $this->fileUpload->save($request->file('image'));
        }

        return $this->repository->create($data);
    }

    public function update(Request $request, string $id)
    {
        $user = Article::find($id);
        $data = $request->all();

        if (!empty($request->file('image', null))) {
            if (!empty($user->image)) {
                $data['image'] = $this->fileUpload->save(
                    $request->file('image'),
                    null,
                    null,
                    'update',
                    $user->image
                );
            } else {
                $data['image'] = $this->fileUpload->save($request->file('image'));
            }
        }

        return $this->repository->update($id, $data);
    }

    public function destroy(string $id)
    {
        // delete the image files
        $user = Article::find($id);
        $this->fileUpload->deleteImage($user->image);

        // delete the record from the database
        return parent::destroy($id);
    }
}
