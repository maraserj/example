<?php
namespace App\Gateways;

use App\Repositories\BaseRepository as Repository;
use Illuminate\Http\Request;

abstract class Gateway
{
    protected $repository;

    /**
     * Service constructor.
     *
     * @param Repository $repository
     */
    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Return a collection of records
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function get(Request $request)
    {
        return $this->repository->paginate($request->get('perPage', 15));
    }

    /**
     * Return a single record
     *
     * @param string $id
     *
     * @return mixed
     */
    public function show(string $id)
    {
        return $this->repository->find($id);
    }

    /**
     * Store a record
     *
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        return $this->repository->create($request->all());
    }

    /**
     * Update an existing record
     *
     * @param Request $request
     * @param string  $id
     *
     * @return mixed
     */
    public function update(Request $request, string $id)
    {
        return $this->repository->update($id, $request->all());
    }

    /**
     * Destroy an existing record
     *
     * @param string $id
     *
     * @return mixed
     */
    public function destroy(string $id)
    {
        return $this->repository->delete($id);
    }
}
