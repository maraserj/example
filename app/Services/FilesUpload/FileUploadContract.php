<?php
namespace App\Services\FileUpload;

interface FileUploadContract
{
    /**
     * @param $file
     *
     * @return mixed
     */
    public function save($file);

    /**
     * @param $filePath
     *
     * @return mixed
     */
    public function delete($filePath);
}