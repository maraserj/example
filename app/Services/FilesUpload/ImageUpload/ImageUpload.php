<?php
namespace App\Services\FileUpload\ImageUpload;
use App\Services\FileUpload\FileUploadContract;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Intervention\Image\ImageManagerStatic as Image;
use Storage;

class ImageUpload implements FileUploadContract
{
    protected $settingPath = 'settings';
    protected $categoriesPath = 'categories';
    protected $channelsPath = 'channels';
    protected $avatarsPath = 'avatars';

    public $baseFolder = 'other';

    /**
     * @param UploadedFile $uploadedImage
     * @param null         $width
     * @param null         $height
     * @param string       $method
     * @param string       $oldImage
     * @param null         $imageName
     *
     * @return null|string
     */
    public function save($uploadedImage = null, $width = null, $height = null, $method = 'save', $oldImage = '', $imageName = null)
    {
        $allowedExtensions = [
            'png',
            'jpg',
            'jpeg',
            'gif',
            'svg',
        ];
        if ($method == 'update') {
            Storage::disk('public')->delete(str_replace('/storage', '', $oldImage));
        }
        $image = null;

        $mime = null;
        if ($uploadedImage instanceof UploadedFile) {
            $name = $uploadedImage->getClientOriginalName();
            $extension = $uploadedImage->getClientOriginalExtension();
            $mime = $uploadedImage->getClientMimeType();
            if (!in_array($extension, $allowedExtensions)) {
                $extension = 'png';
            }
        } else {
            $extension = last(explode('.', $uploadedImage));
            if (!in_array($extension, $allowedExtensions)) {
                $extension = 'png';
            }
            $name = last(str_replace($extension, '', explode('/', $uploadedImage)));
        }
        $path = 'images/'. $this->baseFolder;
        if ($mime === 'image/svg+xml') {
            $extension = 'svg';
            if (!Storage::disk('public')->exists($path)) {
                Storage::disk('public')->makeDirectory($path);
            }
            $path = 'public/images/'. $this->baseFolder;

            $image = Storage::putFileAs($path, $uploadedImage, $imageName ? $imageName . '.' . $extension : $this->makeImageName($name, $extension));

            return $image ? str_replace('public/', 'storage/', $image) : null;
        }

        if (!Storage::disk('public')->exists($path)) Storage::disk('public')->makeDirectory($path);

        $imageInstance = Image::make($uploadedImage);

        if ((isset($width) && $width != '') && (isset($height) && $height != '')) {
            $imageName = $imageName ? $imageName . '.' . $extension : $this->makeImageName($name, $extension, $width, $height);
            $image = $imageInstance
                ->resize((int)$width, (int)$height)
                ->save(storage_path('app/public/' . $path . '/' . $imageName));
        } elseif (isset($width) && $width != '' && ($imageInstance->width() > $width)) {
            $imageName = $imageName ? $imageName . '.' . $extension : $this->makeImageName($name, $extension, $width);
            $image = $imageInstance
                ->widen((int)$width)
                ->save(storage_path('app/public/' . $path . '/' . $imageName));
        } else {
            $imageName = $imageName ? $imageName . '.' . $extension : $this->makeImageName($name, $extension);
            $image = $imageInstance
                ->save(storage_path('app/public/' . $path . '/' . $imageName));
        }
        return $image ? '/storage/' . $path .'/' . $imageName : null;
    }

    public function deleteImage($path)
    {
        if (empty($path)) {
            return false;
        }

        $path = str_replace('/storage', '', $path);
        return Storage::disk('public')->delete($path);
    }

    /**
     * @param $name
     * @param $extension
     * @param null $width
     * @param null $height
     * @return string
     */
    private function makeImageName($name, $extension, $width = null, $height = null): string
    {
        if ($width && $height) {
            return str_replace($extension, '', str_slug($name)) . '_' . time() . '_'. $width . 'x'. $height . '.' . $extension;
        } elseif ($width) {
            return str_replace($extension, '', str_slug($name)) . '_' . time() . '_'. $width . '.' . $extension;
        } else {
            return str_replace($extension, '', str_slug($name)) . '_' . time() . '_original.' . $extension;
        }
    }

    /**
     * @param $filePath
     *
     * @return mixed
     */
    public function delete($filePath)
    {
        // TODO: Implement delete() method.
    }
}
