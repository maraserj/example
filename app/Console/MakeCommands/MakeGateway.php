<?php

namespace App\Console\MakeCommands;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Filesystem\Filesystem;

class MakeGateway extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:gateway {name}';

    protected $type = 'Gateway';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates a Gateway class boilerplate';

    public function __construct(Filesystem $files)
    {
        parent::__construct($files);
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__ . '/stubs/gateway.stub';
    }


    /**
     * @param string $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\\Services\Gateways';
    }
}
