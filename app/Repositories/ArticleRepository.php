<?php

namespace App\Repositories;

use App\Models\Article;

class ArticleRepository extends BaseRepository
{
    protected $model;
    protected $fillable = [];

    public function __construct(Article $model)
    {
        $this->fillable = $model->getFillable();
        return parent::__construct($model);
    }

    public function active()
    {
        return $this->model->active();
    }

    public function getMain()
    {
        return $this->getModel()->parent()->active()->get();
    }

    public function all()
    {
        $categories = $this->model->all();

        return $categories;
    }

    public function paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int $page = null)
    {
        $categories = $this->model->paginate($perPage, $columns, $pageName, $page);

        return $categories;
    }

    public function limit($limit = 15)
    {
        $categories = $this->model->limit($limit);

        return $categories;
    }

    public function getActive()
    {
        return $this->model->active()->get();
    }

    public function getBySlug($slug)
    {
        return $this->model->where('slug', $slug)->with('category')->first();
    }

    public function changeStatus(array $data = [])
    {
        return parent::update($data['id'], array_only($data, $this->fillable));
    }

    public function create(array $data = [])
    {
        return parent::create(array_only($this->checkEmpty($data), $this->fillable));
    }

    public function update($id, array $data = [])
    {
        return parent::update($id, array_only($this->checkEmpty($data), $this->fillable));
    }

    public function delete($id)
    {
        return parent::delete($id);
    }

}
