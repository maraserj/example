<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository
{
    protected $_fillable = [];
    protected $validationRules = [];
    protected $model;


    public function __construct(Model $model)
    {
        $this->_fillable = $model->getFillable();
        $this->model = $model;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function with($array)
    {
        return $this->model->with($array);
    }

    public function all()
    {
        return $this->model->all();
    }

    public function find($id)
    {
        return $this->model->find($id);
    }

    public function findBy($column, $value)
    {
        return $this->model->where($column, $value)->first();
    }

    public function findOrFail($id)
    {
        return $this->model->findOrFail($id);
    }

    public function query()
    {
        return $this->model->query();
    }

    public function count()
    {
        return $this->model->count();
    }

    public function instance(array $attributes = [])
    {
        $model = $this->model;

        return new $model($attributes);
    }

    public function where($column, string $operator = null, $value = null, string $boolean = 'and')
    {
        return $this->model->where($column, $operator, $value, $boolean);
    }

    public function whereIn($key, $whereIn)
    {
        return $this->model->whereIn($key, $whereIn);
    }

    public function paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int $page = null)
    {
        return $this->model->paginate($perPage, $columns, $pageName, $page);
    }

    public function create(array $data = [])
    {
        return $this->model->create($data);
    }

    public function update($id, array $data = [])
    {
        $instance = $this->findOrFail($id);
        $instance->update($data);

        return $instance;
    }

    public function delete($id)
    {
        $model = $this->findOrFail($id);
        $model->delete();

        return $model;
    }

    protected function checkEmpty(array $data = [])
    {
        foreach ($data as $key => $item) {
            switch ($item) {
                case empty($item) || $item == 'null':
                    $data[$key] = null;
                    break;
                case empty($item) || $item == 'false' :
                    $data[$key] = false;
                    break;
                case $item == 'true' :
                    $data[$key] = true;
                    break;
                case $item == '0' || $item == '1' :
                    $data[$key] = (int) $item;
                    break;
            }
        }
        if (!empty($data['title'])) {
            $data['slug'] = str_slug($data['title']);
        }
        if (!empty($data['is_active'])) {
            if ($data['is_active'] === true) {
                $data['is_active'] = 1;
            } else if (empty($data['is_active']) || $data['is_active'] == 'false' || $data['is_active'] == 'null') {
                $data['is_active'] = 0;
            } else {
                $data['is_active'] = 1;
            }
        } else {
            $data['is_active'] = 0;
        }

        return $data;
    }
}
