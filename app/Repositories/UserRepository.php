<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository extends BaseRepository
{

    protected $model;
    protected $role;
    private $filterCollection;

    public function __construct(User $model)
    {
        $this->model = $model;
        parent::__construct($model);
    }

    public function paginate(int $perPage = null, array $columns = ['*'], string $pageName = 'page', int $page = null)
    {
        $categories = $this->model->paginate($perPage, $columns, $pageName, $page);

        return $categories;
    }

    public function all(array $data = [])
    {
        $page = $data['perPage'] ?? 25;
        $this->filterCollection = $this->model->with('role');

        if (!empty($data)) {
            $this->filterCollection->where($this->addWhereParams($data));

            if (!empty($data['sortBy']) && !empty($data['sortTo'])) {
                $this->filterCollection->orderBy($data['sortBy'], $data['sortTo']);
            }
        }

        return $this->filterCollection->paginate($page);
    }

    public function getById($id)
    {
        $user = $this->model->where('id', $id)->first();

        return $user;
    }

    public function findByEmail($email)
    {
        return $this->model->where('email', $email)->first();
    }

    public function create(array $data = [])
    {
        if (isset($data['password'])) {
            $data['password'] = bcrypt($data['password']);
        }

        return parent::create(array_only($data, $this->_fillable));
    }

    public function storeSocial($user = null, array $attributes = [])
    {
        if (!$user instanceof User && is_numeric($user)) {
            $user = $this->findOrFail($user);
        }
        $userSocial = $user->socials()->firstOrNew(['provider' => 'facebook']);
        $userSocial->fill($attributes);

        return $userSocial->save();
    }

    public function updateSocial($user = null, array $attributes = [])
    {

    }

    public function update($id, array $data = [])
    {
        if (!empty($data['password'])) {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']);
        }

        return parent::update($id, array_only($data, $this->_fillable));
    }

    public function delete($id)
    {
        return parent::delete($id);
    }

    private function addWhereParams(array $data = [])
    {
        if (empty($data)) {
            return [];
        }
        $params = [];

        $whereParams = array_only($data, $this->_fillable);
        foreach ($whereParams as $key => $fillParam) {
            if ($key == 'title' && !empty($fillParam)) {
                $params[] = ['title', 'LIKE', '%' . trim($fillParam) . '%'];
            } elseif ($key == 'description' && !empty($fillParam)) {
                $params[] = ['description', 'LIKE', '%' . $fillParam . '%'];
            } elseif (!empty($fillParam)) {
                $params[] = [$key, '=', $fillParam];
            }
        }

        $searchBy = array_get($data, 'searchBy', null);
        $searchText = array_get($data, 'searchText', null);

        if ($searchBy && $searchText && in_array($searchBy, $this->_fillable)) {
            $params[] = [$searchBy, 'LIKE', '%' . $searchText . '%'];
        }

        return $params;
    }

}