<?php
namespace App\Traits;

use Illuminate\Http\Response as IlluminateResponse;

trait ApiResponse
{
    protected static $response = null;

    protected $statusCode = IlluminateResponse::HTTP_OK;

    /**
     * @param $statusCode
     * @return $this
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return  $this->statusCode;
    }

    /**
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondForbidden($message = 'Forbidden!')
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_FORBIDDEN)->respondWithError($message);
    }

    /**
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondInternalError($message = 'Internal Error!')
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR)->respondWithError($message);
    }

    /**
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondInvalidParameters($message = 'Invalid Parameters!')
    {
        return $this->setStatusCode(IlluminateResponse::HTTP_UNPROCESSABLE_ENTITY)->respondWithError($message);
    }

    /**
     * @param $message
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithError($message)
    {
        return $this->respond([
            'error' => ['message' => $message, 'status_code' => $this->getStatusCode()]
        ]);
    }


    /**
     * @param int $code
     * @param null $status
     * @param null $data
     * @param null $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respond($code = 200, $status = null, $data = null, $message = null)
    {
        $return = null;
        if (!is_null($status) || !is_null($message) || !is_null($data)) {
            $return  = [];
            if (! is_null($status))  $return['status'] = $status;
            if (! is_null($message)) $return['message'] = $message;
            if (! is_null($data))    $return['data']   = $data;
        }
        return response()->json($return, $code);
    }

    /**
     * @param null $data
     * @param null $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondOK($data = null, $message = null)
    {
        return $this->respond(200, 'OK', $data, $message);
    }

    /**
     * @param null $data
     * @param null $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondUnchanged($data = null, $message = null) {
        return $this->respond(200, 'UNCHANGED', $data, $message);
    }

    /**
     * @param null $data
     * @param null $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondCreated($data = null, $message = null) {
        return $this->respond(201, 'CREATED', $data, $message);
    }

    /**
     * @param null $message
     * @param null $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondDenied($message = null, $data = null) {
        $message = $message ?? trans('common.errors.permission_denied');
        return $this->respond(403, $message, $message, $data);
    }

    /**
     * @param null $errorMessage
     * @param string $status
     * @param null $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondBadRequest($errorMessage = null, $status = 'BAD_REQUEST', $data = null) {
        $errorMessage = is_null($errorMessage) ? trans('common.errors.bad_request') : $errorMessage;
        return $this->respond(400, $status, $data, $errorMessage);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondUnauthorized() {
        return $this->respond(401, 'UNAUTHORIZED', null, trans('common.errors.401_bad_permissions'));
    }

    /**
     * @param null $errorMessage
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondNotFound($errorMessage = null) {
        $errorMessage = is_null($errorMessage) ? trans('common.errors.404') : $errorMessage;
        return $this->respond(404, 'NOT_FOUND', null, $errorMessage);
    }

    /**
     * @param $data
     * @param string $status
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondConflict($data, $status = 'EMAIL_EXISTS') {
        $msg = implode("\n", $data->all());
        return $this->respond(409, $status, $data, $msg);
    }

    /**
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondInvalidation($data) {
        $msg = implode("\n", $data->all());
        return $this->respond(422, 'VALIDATION_ERROR', $data, $msg);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondUnexpected() {
        return $this->respond(500, 'UNEXPECTED_ERROR', null, trans('common.errors.500'));
    }

    public function respondLocked() {
        return $this->respond(423, 'Locked', null, trans('common.calendars.locked'));
    }
}
