<?php

namespace App\Http\Requests\User;

use App\Models\Setting;
use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ];

        $invite_codes = [];

        $forever_code = config('app.forever_invite_code');
        if ($forever_code) {
            $invite_codes[] = $forever_code;
        }

        $temp_code = optional(Setting::where('code', 'temp_invite_code')->first())->value;
        if ($temp_code) {
            $invite_codes[] = $temp_code;
        }

        if (!empty($invite_codes)) {
            $rules['invite_code'] = 'required|in:'.implode($invite_codes, ',');
        }
        else {
            $rules['invite_code'] = 'required';
        }

        return $rules;
    }
}
