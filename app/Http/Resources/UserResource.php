<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $array = parent::toArray($request);
        $array['created_at'] = $this->created_at->format('d.m.Y');
        unset($array['updated_at']);

        $array['avatar'] = $array['avatar'] ?? asset('images/noavatar.png');

        return $array;
    }
}
