<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $array = parent::toArray($request);
        $array['is_active'] = (bool) $this->is_active;
        $array['created_at'] = $this->created_at->format('d.m.Y');
        $array['updated_at'] = $this->updated_at->format('d.m.Y');
        $array['children'] = CategoryResource::collection($this->whenLoaded('children'));
        $array['articles'] = ArticleResource::collection($this->whenLoaded('articles'));

        $array['image'] = $array['image'] ?? asset('images/noimage.png');
        $array['icon'] = $array['icon'] ?? asset('images/noimage.png');

        return $array;
    }
}
