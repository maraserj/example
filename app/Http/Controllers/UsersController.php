<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use App\Http\Requests\User\{
    Destroy, Store, Update
};
use App\Http\Resources\UserResource as Resource;
use App\Gateways\UsersGateway as Service;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    private $service;

    /**
     * UserController constructor.
     *
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * List the Users
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $users = $this->service->get($request);

        return (Resource::collection($users))->response();
    }

    /**
     * Show a User
     *
     * @param string $id
     *
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        $user = $this->service->show($id);

        return (new Resource($user))->response();
    }

    /**
     * Persist a User
     *
     * @param Store $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Store $request): JsonResponse
    {
        $user = $this->service->store($request);

        return (new Resource($user))->response();
    }

    /**
     * Update an existing User
     *
     * @param Update $request
     * @param        $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Update $request, string $id): JsonResponse
    {
        $user = $this->service->update($request, $id);

        return (new Resource($user))->response();
    }

    /**
     * Remove a User
     *
     * @param Destroy $request
     * @param         $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Destroy $request, string $id): JsonResponse
    {
        $user = $this->service->destroy($id);

        return (new Resource($user))->response();
    }
}
