<?php

namespace App\Http\Controllers\Admin;

use App\Services\FileUpload\ImageUpload\ImageUpload;
use Exception;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;

class IndexController extends Controller
{
    /**
     * @var ImageUpload
     */
    private $imageUpload;

    public function __construct(ImageUpload $imageUpload)
    {
        $this->imageUpload = $imageUpload;
    }

    public function users()
    {
        return view('users.index');
    }

    public function categories()
    {
        return view('categories.index');
    }

    public function categoryShow()
    {
        return view('categories.show');
    }

    public function operationShow()
    {
        return view('operations.show');
    }

    public function articles()
    {
        return view('articles.index');
    }

    public function articleShow()
    {
        return view('articles.show');
    }

    public function settings()
    {
        return view('settings.index');
    }

    public function getIcons()
    {
        $icons = collect(Storage::files('public/images/icons'))
            ->map(function ($item) {
                return '/' . str_replace('public', 'storage', $item);
            })
            ->toArray();

        return $icons;
    }

    public function saveIcon(Request $request)
    {
        $this->imageUpload->baseFolder = 'icons';
        $imagePath = null;
        if (!empty($request->file('image'))) {
            $imagePath = $this->imageUpload->save($request->file('image'), 200, 200);
        }

        return $this->respondOK($imagePath);
    }

    public function saveArticleImage(Request $request)
    {
        $this->imageUpload->baseFolder = 'articles';
        $imagePath = null;
        $error = null;
        if (!empty($request->file('upload'))) {
            try {
                $imagePath = $this->imageUpload->save($request->file('upload'));
            } catch (Exception $e) {
                $error = $e->getMessage();
            }
        }

        if ($imagePath) {
            return response()->json([
                'status' => 'success',
                'url' => asset($imagePath),
                'uploaded' => 1,
                'fileName' => last(explode('/', $imagePath))
            ]);
        }

        return response()->json(['status' => 'error', 'message' => 'Whoops! Error loading image.', 'error' => $error], 500);

    }

}
