<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use App\Http\Requests\Setting\{
    Destroy, Store, Update
};
use App\Http\Resources\SettingResource as Resource;
use App\Services\Gateways\SettingsGateway as Service;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    private $service;

    /**
     * SettingController constructor.
     *
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * List the Settings
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $settings = $this->service->get($request);

        return (Resource::collection($settings))->response();
    }

    /**
     * Show a Setting
     *
     * @param string $id
     *
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        $setting = $this->service->show($id);

        return (new Resource($setting))->response();
    }

    /**
     * Persist a Setting
     *
     * @param Store $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Store $request): JsonResponse
    {
        $setting = $this->service->store($request);

        return (new Resource($setting))->response();
    }

    /**
     * Update an existing Setting
     *
     * @param Update $request
     * @param        $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Update $request, string $id): JsonResponse
    {
        $setting = $this->service->update($request, $id);

        return (new Resource($setting))->response();
    }

    /**
     * Remove a Setting
     *
     * @param Destroy $request
     * @param         $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Destroy $request, string $id): JsonResponse
    {
        $setting = $this->service->destroy($id);

        return (new Resource($setting))->response();
    }
}
