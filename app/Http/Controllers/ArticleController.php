<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use App\Http\Requests\Article\{
    Destroy, Store, Update
};
use App\Http\Resources\ArticleResource as Resource;
use App\Gateways\ArticlesGateway as Service;
use Illuminate\Http\Request;
class ArticleController extends Controller
{
    private $service;

    /**
     * ArticleController constructor.
     *
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * List the Articles
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $articles = $this->service->get($request);

        return (Resource::collection($articles))->response();
    }

    /**
     * Show a Article
     *
     * @param string $id
     *
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        $article = $this->service->show($id);

        if (!$article) {
            return $this->respondNotFound();
        }

        return (new Resource($article))->response();
    }

    /**
     * Persist a Article
     *
     * @param Store $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Store $request): JsonResponse
    {
        $article = $this->service->store($request);

        return (new Resource($article))->response();
    }

    /**
     * Update an existing Article
     *
     * @param Update $request
     * @param        $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Update $request, string $id): JsonResponse
    {
        $article = $this->service->update($request, $id);

        return (new Resource($article))->response();
    }

    /**
     * Remove a Article
     *
     * @param Destroy $request
     * @param         $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Destroy $request, string $id): JsonResponse
    {
        $article = $this->service->destroy($id);

        return (new Resource($article))->response();
    }
}
