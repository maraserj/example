<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('privacy', 'getAccessToken', 'emailRequest');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return redirect()->route('admin.categories');
    }

    /**
     * Show privacy page.
     *
     * @return \Illuminate\Http\Response
     */
    public function privacy()
    {
        return view('privacy');
    }

    public function getAccessToken(Request $request)
    {
        $this->validate($request, [
            'username' => 'email|required',
            'password' => 'required',
        ]);

        $response = (new \GuzzleHttp\Client)->post('https://intelweb.xyz/oauth/token', [
            'form_params' => [
                'grant_type'    => 'password',
                'client_id'     => 2,
                'client_secret' => 'gajUGPiZ9LKR8OEnMrMl7XeyDqieVq70hLPe9T0u',
                'username'      => $request->get('username'),
                'password'      => $request->get('password'),
            ],
            'headers' => [
                'Accept' => 'application/json'
            ],
            'http_errors' => false,
        ]);

        return response()->json(json_decode((string)$response->getBody(), true), $response->getStatusCode());
    }

    public function emailRequest(Request $request)
    {
        session()->flash('success', 'Запрос отправлен. Спасибо');

        return back();
    }
}
