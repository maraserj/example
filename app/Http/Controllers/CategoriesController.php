<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use App\Http\Requests\Category\{
    Destroy, Store, Update
};
use App\Http\Resources\CategoryResource as Resource;
use App\Gateways\CategoriesGateway as Service;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    private $service;

    /**
     * UserController constructor.
     *
     * @param Service $service
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }

    /**
     * List the Users
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $categories = $this->service->get($request);

        return (Resource::collection($categories))->response();
    }

    /**
     * Show a User
     *
     * @param string $id
     *
     * @return JsonResponse
     */
    public function show(string $id): JsonResponse
    {
        $category = $this->service->show($id);

        if (!$category) {
            return $this->respondNotFound();
        }

        return (new Resource($category))->response();
    }

    /**
     * Persist a User
     *
     * @param Store $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Store $request): JsonResponse
    {
        $category = $this->service->store($request);

        return (new Resource($category))->response();
    }

    /**
     * Update an existing User
     *
     * @param Update $request
     * @param        $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Update $request, string $id): JsonResponse
    {
        $category = $this->service->update($request, $id);

        return (new Resource($category))->response();
    }

    /**
     * Remove a User
     *
     * @param Destroy $request
     * @param         $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Destroy $request, string $id): JsonResponse
    {
        $category = $this->service->destroy($id);

        return $this->respondOK(['deleted' => $category]);
    }
}
